class Profile < ActiveRecord::Base
	belongs_to :user
	has_many :occupations
	
  	mount_uploader :photo, PhotoUploader

  	before_save :set_status

  	def set_status
  		self.status = "inactive" if self.status.nil?
  	end


end