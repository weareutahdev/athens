class User < ActiveRecord::Base
	has_one :profile
	devise 	:database_authenticatable, :registerable, :recoverable, :rememberable, 
			:trackable, :authentication_keys => [:login]
end