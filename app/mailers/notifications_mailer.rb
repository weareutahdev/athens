class NotificationsMailer < ActionMailer::Base

  default :from => "noreply@weareutahtech.com"
  default :to => "smiles.seth@gmail.com"

  def new_message(message)
    @message = message
    mail(:subject => "Contact Form Submission")
  end

end