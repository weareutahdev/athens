class StaticPagesController < ApplicationController
  def home
  end

  def about
  end

  def join
  end
  
  def watermark
  end

  def faq
  end

  def terms
  end
end
