define(['backbone'], function(Backbone){

	var JoinView = Backbone.View.extend({
		
		template:'<div class="join-view">This is the join view</div>',

		initialize: function () {
			this.$el.append($(this.template));
			this.render();
		},

		events: {
			'click .submit':'onClickSubmit'
		},

		render: function () {
			_.defer(_.bind(function(){
				this.$el.find('select').customSelect();	
			}, this));
		},

		destroy: function () {

		},

		onClickSubmit: function () {
			var data = {
				bio: this.$el.find('.bio'),
				headline: this.$el.find('.headline')

			}


			if(this.validateData()){

			}
		},

		validateData: function(){
			return true;
		}
	})

	return JoinView;

});