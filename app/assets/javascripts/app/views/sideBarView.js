define(['backbone',
		'moment',
		'underscore',
		'jquery',
		'text!/assets/app/templates/sideBarTemplate.html'],

		function (Backbone, moment, _, $, SideBarTemplate) {

			var SideBarView = Backbone.View.extend({

				className: 'side-bar',

				template: SideBarTemplate,

				events: {
					'click .close': 'onClickClose'
				},
				
				initialize: function () {
					this.currentView = this.previousView = {cid:null};
					this.render();
				},

				render: function () {
					this.$el.append(this.template);
					this.body = this.$el.find('.body');
					this.button = this.$el.find('.close');
				},

				appendView: function (view) {
					this.previousView = this.currentView;
					this.currentView = view;
					if(this.currentView.cid == this.previousView.cid){ 
						this.openSidebar();
						return; 
					}
					this.body.fadeOut('fast', _.bind(function(){
						this.body.empty();
						this.body.append(view.$el);
						this.body.fadeIn('fast');
					}, this));
					
				},

				openSidebar: function () {
					if(this.button.text() != 'close'){
						this.toggleSidebar();
					}
				},

				closeSidebar: function () {
					if(this.button.text() == 'close'){
						this.toggleSidebar();
					}
				},

				toggleSidebar: function () {
					this.body.stop(true);
					this.button.stop(true);

					if(this.button.text() == 'close'){
						this.button.text('open');
						this.$el.animate({
							width:'0px'
						});
						this.$el.find('.footer').animate({
							width:'0px'
						});
						this.$el.find('.content').animate({
							width:'0px'
						});
						this.options.appView.masonView.$el.animate({
							'padding-left':'0px'
						})
					} else {
						this.button.text('close');
						this.$el.animate({
							width:'280px'
						});
						this.$el.find('.footer').animate({
							width:'280px'
						});
						this.$el.find('.content').animate({
							width:'280px'
						});
						this.options.appView.masonView.$el.animate({
							'padding-left':'280px'
						});
					}
					this.options.appView.masonView.reload();
				},

				onClickClose: function () {
					this.toggleSidebar();
				}

			});

			return SideBarView;

});