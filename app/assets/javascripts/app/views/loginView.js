define(['backbone',
		'underscore',
		'views/scrimView',
		'views/joinView'], 

		function(Backbone, _, ScrimView, JoinView){

			var LoginView = Backbone.View.extend({
				
				className: 'login-view',

				template: _.template(
					'<div class="join">{{ name }}</div>' +
					'<input type="text" placeholder="search">'
				),

				events: {
					'click .join':'onClickJoin',
					'keyup input':'onKeyupInput'
				},

				initialize: function () {
					this.render();
				},

				render: function () {
					var info = {
						name: 'join/login'
					}
					if(this.model){
						info.name = this.model.get('name');
					}
					this.$el.append(this.template(info));
				},

				onClickJoin: function () {
					this.scrimView = new ScrimView({});
					this.joinView = new JoinView({});
					this.scrimView.openScrim(this.joinView);
				},

				onKeyupInput: function () {
					console.log('searchign!');
				}
			})

			return LoginView;

});