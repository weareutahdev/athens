define(['backbone',
		'underscore',
		'text!/assets/app/templates/detailViewTemplate.html'], 

		function (Backbone, _, DetailViewTemplate){

			var SideBarDetailView = Backbone.View.extend({

				className: 'detail-view',

				template: _.template(DetailViewTemplate),

				events: {
					'click .close':'onClickClose'
				},
				
				initialize: function () {
					
				},

				render: function () {

				},

				renderModel: function (model) {
					this.$el.empty();
					this.$el.append(this.template(model.toJSON()));
				},

				destroy: function () {
					this.$el.remove();
				},

				onClickClose: function (){
					this.trigger('close');
				}

			});

			return SideBarDetailView;

});