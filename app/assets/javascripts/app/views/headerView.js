define(['backbone', 
		'underscore',
		'jquery',
		'text!/assets/app/templates/headerViewTemplate.html',
		'views/sideBarAboutView',
		'views/sideBarSearchView'], 

	function (Backbone, _, $, HeaderViewTemplate, SideBarAboutView, SideBarSearchView) {
	
		var HeaderView = Backbone.View.extend({

			className: 'header',

			template: _.template(HeaderViewTemplate),

			events:{
				'click .logo':'onLogoClick',
				'click .login':'onLoginClick',
				'click .find':'onFindClick',
				'click .about':'onAboutClick'
			},

			initialize: function () {
				this.sideBar = this.options.appView.sideBarView;
				this.render();
			},

			render: function () {
				this.$el.empty();
				this.$el.append(this.template({ login: window.user ? window.user.name : 'Login' }));
				this.sideBar.appendView( new SideBarAboutView({}) );
			},

			loadSidebar: function (type) {
				var view;
				switch(type){
					case 'search': view = this.sideBarSearchView ? this.sideBarSearchView : new SideBarSearchView({});
								break;
					case 'about': if(this.sideBarAboutView){
										view = this.sideBarAboutView
									} else {
										this.sideBarAboutView = new SideBarAboutView({});
										view = this.sideBarAboutView;
									}
								break;
				}
				this.sideBar.appendView(view);
			},

			onLogoClick: function () {
				this.loadSidebar('home');
			},

			onLoginClick: function () {
				window.location.pathname = '/users/sign_in';
			},

			onFindClick: function (evt) {
				this.$el.find('.selected').removeClass('selected')
				$(evt.target).addClass('selected');
				this.loadSidebar('search');
			},

			onAboutClick: function (evt) {
				this.$el.find('.selected').removeClass('selected')
				$(evt.target).addClass('selected');
				this.loadSidebar('about');
			}

		});

	return HeaderView;
});