define(['backbone',
		'jquery'], 

	function(Backbone, $){

		var ScrimView = Backbone.View.extend({

			id:'scrim',

			events:{
				'click':'closeScrim'
			},
			
			initialize: function () {
				$('body').prepend(this.$el);
				this.scrim = $('<div class="scrim"></div>').appendTo(this.$el);
				this.innerScrim = $('<div class="inner-scrim"></div>').appendTo(this.$el);
			},

			openScrim: function (view) {
				this.view = view;
				this.view.on('close', this.closeScrim);
				this.innerScrim.append(view.$el);
				this.$el.fadeIn();
			},

			closeScrim: function () {
				var that = this;
				this.$el.fadeOut(function(){
					that.destroy();	
				});
			},

			destroy: function () {
				this.view.destroy();
				this.remove();
			}

		});

		return ScrimView;

});