define(['backbone',
		'jquery',
		'collections/profileCollection',
		'views/headerView',
		'views/masonView',
		'views/sideBarView',
		'views/loginView'], 

		function (Backbone, $, ProfileCollection, HeaderView, MasonView, SideBarView, LoginView) {

			var AppView = Backbone.View.extend({

				id:'app',

				initialize: function() {
					this.collection = new ProfileCollection(window.data);
					// this.collection.fetch({
					// 	success: _.bind(function(){
					// 		this.render();
					// 	}, this)
					// });
					this.render();
				},

				render: function () {

					this.sideBarView = new SideBarView({ appView: this });
					this.headerView = new HeaderView({ appView: this });
					this.masonView = new MasonView({collection: this.collection, appView: this });

					this.$el.append(this.headerView.$el);
					this.$el.append(this.sideBarView.$el);
					this.$el.append(this.masonView.$el);

					$('body').append(this.$el);
				}

			});

			return AppView;

});