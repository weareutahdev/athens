define(['backbone',
		'jquery',
		'underscore',
		'moment',
		'text!/assets/app/templates/sideBarTweetTemplate.html',
		'text!/assets/app/templates/sideBarAboutTemplate.html',
		'jquery.customSelect'], 

		function(Backbone, $, _, moment, SideBarTweetTemplate, SideBarAboutTemplate){
	
			var SideBarHomeView = Backbone.View.extend({

				tweet: _.template(SideBarTweetTemplate),

				initialize: function () {
					this.buildTwitter = _.bind(this.buildTwitter, this);
					this.render();
				},

				render: function () {
					this.$el.append(SideBarAboutTemplate);
					this.getTweets();
					_.defer(_.bind(function(){
						this.$el.find('select').customSelect();	
					}, this));
				},

				getTweets: function () {
					var req = $.ajax({
						url:'https://api.twitter.com/1/statuses/user_timeline.json?include_entities=true&include_rts=true&screen_name=weareutahtech&count=3',
						dataType: 'jsonp'
					})
					req.done(this.buildTwitter);
				},

				buildTwitter: function (json) {
					var twitter = '';
					_.each(json, _.bind(function(tweet){
						twitter += this.buildTweet(tweet);
					}, this));
					this.$el.find('.twitter').append($(twitter));
				},

				buildTweet: function (tweet) {
					return this.tweet({
						username: 'WeAreUtahTech',
						tweet: tweet.text,
						timestamp: moment(tweet.created_at).format('D MMM')
					})
				}

			});

		return SideBarHomeView;

})