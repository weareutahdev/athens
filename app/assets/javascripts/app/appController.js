
define(['views/appView'], 

	function(AppView){

		function AppController () {
			console.log('app controller constructor');
			appView = new AppView();
		}

		return AppController;

});
