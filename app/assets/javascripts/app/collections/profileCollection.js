define(['backbone',
		'models/profileModel'], function(Backbone, Profile){
	
	var profileCollection = Backbone.Collection.extend({

		model: Profile,

		url:'/profiles.json',

		initialize: function () {

		}
	});

	return profileCollection;

});