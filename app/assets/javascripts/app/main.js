console.log('running main');

requirejs.config({
	
	paths:{
		'text': 'vendors/text',
		'backbone': 'vendors/backbone',
		'underscore': 'vendors/underscore',
		'jquery': 'vendors/jquery',
		'moment': 'vendors/moment',
		'jquery.customSelect': 'vendors/customSelect',
		'jquery.masonry': 'vendors/masonry',
	},

	shim:{
		
		'jquery':{
			exports: '$',
			init: function(something){
				console.log('jquery init');
			}
		},

		'underscore':{
			exports: '_',
			init: function(){
				var underscore = window._
				underscore.templateSettings = {
					interpolate : /\{\{([\s\S]+?)\}\}/g,
					evaluate: /\[\[(.+?)\]\]/g
				};
				console.log('underscore init');
				delete window._;
				return underscore;
			}
		},

		'backbone':{
			deps:['underscore','jquery'],
			exports: 'Backbone',
			init: function(){
				var backbone = window.Backbone;
				console.log('init backbone running');
				delete window.Backbone;
				return backbone;
			}
		},

		'moment':{
			exports: 'moment',
			init: function(){
				var moment = window.moment;
				delete window.moment;
				console.log('moment init');
				return moment;
			}
		},
		
		'jquery.customSelect': {
            deps: ['jquery'],
            exports: 'jQuery.fn.customSelect',
            init: function(){
				console.log('customSelect init');
			}
        },
        
        'jquery.masonry': {
            deps: ['jquery'],
            exports: 'jQuery.fn.masonry',
            init: function(){
				console.log('masonry init');
			}
        },
	}
});

require(['appController'], function(AppController){
	delete window.jQuery;
	delete window.$;
	delete window.moment;
	var ac = AppController();
});