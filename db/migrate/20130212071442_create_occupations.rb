class CreateOccupations < ActiveRecord::Migration
  def change
    create_table :occupations do |t|
      t.string 	:name
      t.integer :profile_id 
      
      t.timestamps
    end
  end
end
