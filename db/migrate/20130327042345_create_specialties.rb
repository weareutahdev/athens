class CreateSpecialties < ActiveRecord::Migration
  def change
    create_table :specialties do |t|
      t.string 	:name
      t.integer	:occupation_id
      t.integer :profile_id

      t.timestamps
    end
  end
end
