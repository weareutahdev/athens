class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string  :headline
      t.string  :title
      t.string  :employer
      t.text    :bio
      t.date    :birthday
      t.string  :status
      t.string  :first_name
      t.string  :last_name
      t.string  :contact_email
      t.text    :message

      # foreign key
      t.integer :user_id

      t.timestamps
    end
  end
end
